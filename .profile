
REPO_PATH="${HOME}/repos"

[ -d "${REPO_PATH}" ] && mkdir -p "${REPO_PATH}"

# If mr and .mrconfig file are available and no mr command was already launch (eg: .tmux directory unavailable)
if [ $(command -v mr) ] && [ -f "${HOME}"/.mrconfig ] && [ ! -L "${HOME}"/.tmux ]; then
  mr checkout
fi

# Tmux
[ -d "${REPO_PATH}"/gardouille.tmux ] && [ ! -L "${HOME}"/.tmux ] && rm -f "${HOME}"/.tmux && ln -s "${REPO_PATH}"/gardouille.tmux "${HOME}"/.tmux
[ -L "${HOME}"/.tmux ] && [ ! -L "${HOME}"/.tmux.conf ] && rm -f "${HOME}"/.tmux.conf && ln -s "${HOME}"/.tmux/tmux.conf "${HOME}"/.tmux.conf

# Vim
[ -d "${REPO_PATH}"/gardouille.vim ] && [ ! -L "${HOME}"/.vim ] && rm -f "${HOME}"/.vim && ln -s "${REPO_PATH}"/gardouille.vim "${HOME}"/.vim
[ -L "${HOME}"/.vim ] && [ ! -L "${HOME}"/.vimrc ] && rm -f "${HOME}"/.vimrc && ln -s "${HOME}"/.vim/vimrc "${HOME}"/.vimrc

# Zsh
[ -d "${REPO_PATH}"/101010.zsh ] && [ ! -L "${HOME}"/.zsh ] && rm -rf .zsh/ && ln -s "${REPO_PATH}"/101010.zsh "${HOME}"/.zsh
[ -L "${HOME}"/.zsh ] && [ ! -L "${HOME}"/.zshrc ] && rm -f .zshrc && ln -s "${HOME}"/.zsh/zshrc "${HOME}"/.zshrc

# bin
[ -d "${REPO_PATH}"/gardouille.scripts ] && [ ! -L "${HOME}"/bin ] && rm -rf "${HOME}"/bin && ln -s "${REPO_PATH}"/gardouille.scripts "${HOME}"/bin

# If ZSH is available
if [ $(command -v zsh) ]; then
  exec zsh
fi
